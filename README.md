Sample project that aims to print a Christmas tree on the Standard Output.

To run it, simply import the gradle file with IntelliJ IDEA and run the Main function.

To run the test, just run the TreeAcceptanceTest file instead.