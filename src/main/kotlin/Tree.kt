class Tree(private val height: Int) {
    private var tree: String = EMPTY_TREE
    fun generate(output: Output) {
        if (height <= 0){
            output.out(EMPTY_TREE)
            return
        }
        addTreeBody()
        addRoot()
        output.out(tree)
    }

    private fun addRoot() {
        tree += addSpaces( 1) + "|"
    }

    private fun addTreeBody() {
        for (index in 1..height) {
            tree += addSpaces(index) + addLeaves(index) + "\n"
        }
    }

    private fun addLeaves(index: Int) = "*".repeat((index - 1) * 2 + 1)

    private fun addSpaces(index: Int) = " ".repeat(height - index)

    companion object{
        const val EMPTY_TREE = ""
    }
}