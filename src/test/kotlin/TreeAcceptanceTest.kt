import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Test

/*
Implement a function that prints a Christmas tree on the standard output, according with the following rules:
1. The function must be implemented using an object-oriented language
2. The function must accept a parameter as the tree height
3. The Christmas tree must be made of stars (i.e. * )
4. The Christmas tree must have a root (‘|’) if applicable
5. The function must manage corner cases and unexpected input
Example(4):

   *
  ***
 *****
*******
   |
 */

class TreeAcceptanceTest {
    private val output: Output = mock()

    @Test
    fun treeZeroHeightReturnsEmptyString() {
        Tree(0).generate(output)

        verify(output).out(TREE_HEIGHT_0)
    }

    @Test
    fun treeNegativeHeightReturnsEmptyString(){
        Tree(-1).generate(output)

        verify(output).out(TREE_HEIGHT_0)
    }

    @Test
    fun treeOneHeightReturnsOneStar(){
        Tree(1).generate(output)

        verify(output).out(TREE_HEIGHT_1)
    }

    @Test
    fun happyPath(){
        Tree(4).generate(output)

        verify(output).out(TREE_HEIGHT_4)
    }

    companion object{
        const val TREE_HEIGHT_0 = ""
        const val TREE_HEIGHT_1 = """*
|"""
        const val TREE_HEIGHT_4 = """   *
  ***
 *****
*******
   |"""
    }
}